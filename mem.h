#include <iostream>
#include <fstream>
#include <vector>
class Memory {
	private:
		int vectorToInt(std::vector<int> nums) {
			std::string s;
			for (int n : nums) {
				s = s + (char)n;
			}
			int fin = atoi(s.c_str());
			return fin;
		}
		int getNumber(std::string &rawS) {
			std::vector<int> nums;
			for( char buf : rawS ) {
				if (isdigit(buf)) {
					nums.push_back((int)buf);
				}
			}
			return vectorToInt(nums);
		}
		int proc;
	public: 
		int getUsesMemory() {
			std::ifstream file("/proc/meminfo", std::ios_base::in);
		
			std::string ss;
			getline(file,ss);
			double total = (double)getNumber(ss);
			getline(file,ss);
			getline(file,ss);
			int available = (double)getNumber(ss);
			int proc = (1-available/total)*100;
			Memory::proc = proc;
			return proc;
		}
};